//fra pind 3
"let x = ref 1 in if !x = 1 then x := 2 else 42 end"
> run (fromString "let x = ref 1 in if !x = 1 then x := 2 else 42 end");;
val it : HigherFun.value = Int 2

---

//fra pind 3
"let x = ref 2 in (x := 3) + !x end"
> run (fromString "let x = ref 2 in (x := 3) + !x end");;
val it : HigherFun.value = Int 6

---

"let x = ref 1 in x := !x + 2 end"
// returns 3
> run (fromString "let x = ref 1 in x := !x + 2 end");;
val it : HigherFun.value = Int 3

---

"let f x = !x in let y = ref 2 in f y end end"
//returns 2
> run (fromString "let f x = !x in let y = ref 2 in f y end end");;
val it : HigherFun.value = Int 2

---

"let x = ref 10 in let y = ref x in !y end end"
//returns 20
> run (fromString "let x = ref 10 in let y = ref x in !y end end");;
val it : HigherFun.value = RefVal {contents = Int 10;}

---

"let x = ref 10 in let y = ref x in !(!y) end end"
//returns 10
> run (fromString "let x = ref 10 in let y = ref x in !(!y) end end");;
val it : HigherFun.value = Int 10
