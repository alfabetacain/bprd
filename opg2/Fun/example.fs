Let ("x", Ref (CstI 1),
  UpdRef(Var "x", 
    Prim("+", Deref (Var "x"), CstI 2))
)
// returns 3
> run (Let ("x", Ref (CstI 1), UpdRef(Var "x", Prim("+", Deref (Var "x"), CstI 2))));;
val it : HigherFun.value = Int 3

---

Letfun ("f", "x", Deref(Var "x"),
  Let ("y", Ref (CstI 2),
    Call(Var "f", Var "y")
  )
)
//returns 2
> run (Letfun ("f", "x", Deref(Var "x"), Let ("y", Ref (CstI 2), Call (Var "f", Var "y"))));;
val it : HigherFun.value = Int 2
---

Let ("x", Ref (CstI 10),
  Let ("y", Ref (Var "x"),
    Deref (Var "y")
  )
)
//returns RefVal (ref 10)
> run (Let ("x", Ref (CstI 10), Let ("y", Ref (Var "x"), Deref (Var "y"))));;
val it : HigherFun.value = RefVal {contents = Int 10;}
---

Let ("x", Ref (CstI 10),
  Let ("y", Ref (Var "x"),
    Deref (Deref (Var "y"))
  )
)
//returns 10
> run (Let ("x", Ref (CstI 10), Let ("y", Ref (Var "x"), Deref (Deref (Var "y")))));;
val it : HigherFun.value = Int 10
