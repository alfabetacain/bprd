void main() {
	print 2 .< 3 .< 4;
	print 3 .< 2 .== 2;
	print 3 .> 2 .== 2;
	print (3 .> 2 .== 2) == (3 .> 1 .== 1);
	print (3 .> 2 .== 2) == 1;

	//my changes:
	print 2 .<= 2 .<= 100;
	print 2 .>= 2 .>= 2;
	print 2 .>= 1 .!= 2;
	print 3 .>= 2 .< 10;
	print 3 .!= 2 .== 2;
	print -1 .<= 2 .> -30;
}
